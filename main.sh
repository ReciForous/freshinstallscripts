#!/bin/bash

source scripts/prettify.sh

echo -e  "Welcome to the fresh install script, want to do your first time setup? y/N"

read ans

if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
	echo "install packages? y/N"
	read ans

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		echo -e "${GREEN}Installing packages${NC}"
	else
		echo -e "${LIGHTBLUE}HAH! JK! You dont have an option${NC}"
	fi
	
	# Install regardless
	source scripts/install-packages.sh

	echo -e "Install zsh? y/N ${LIGHTBLUE}NOTE: This is an actual choice${NC}"
	read ans

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-zsh.sh
	fi

	echo "install php? y/N"
	read ans 

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-php.sh
	fi

	echo "install docker? y/N"
	read ans 
	
	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		dockerinstalled="yes"
		source scripts/install-docker.sh
	fi

	echo "install google-chrome? y/N"
	read ans

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-chrome.sh
	fi

	echo "install sublime? y/N"
	read ans

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-sublime.sh
	fi

	echo "install node? y/N"
	read ans

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-node.sh
	fi

	echo "install postman? y/N"
	read ans 

	if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
		source scripts/install-postman.sh
	fi

	if [ "$dockerinstalled" = "yes" ]; then
		echo "Setting up docker no sudo"
		newgrp docker
		sudo groupadd docker
		echo -e "${RED}User will now be added to the docker group and the system will reboot so you can use docker without sudo${NC}"
		echo -e "Press ctrl C if you wish to cancel and exit script, ${LIGHTBLUE}press literally enter to continue${NC}"
		read ans
		sudo usermod -aG docker $USER && reboot
	fi
else
	echo -e "${RED}:( Exiting this awesome install script...${NC}"
	exit
fi
