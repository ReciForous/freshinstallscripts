#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

# source configs.sh
# source prettify.sh

declare -a packages
packages=(
	mysql-server
	mysql-common
	python3
	python3-distutils
	curl
	mpv
	git
	vim
	dnf-core-plugins
	apt-transport-https
	ca-certificates
	gnupg-agent
	software-properties-common 
	tmux
	tree
)

descriptions=(
	"MySQL community server"
	"MySQL libraries"
	"Python3 language"
	"Python dependencies for installing distro free pip"
	"The magical download command, its amazing if you DONT have it on your distro"
	"Oh look, its vlc but better..."
	"Again... How does your distro not have this?"
	"Oh look, its nano... but better"
	"Repo manager for fedora"
	"Repo manager for Ubuntu"
	"Dependencies for repo management"
	"Yet another dependency for repo management"
	"More dependencies for repo management"
	"Background terminal management program"
	"Directory structure viewer"
)

echo "The following packages will be installed!"
for ((i=0; i<${#packages[@]}; ++i)); do
	echo "${packages[i]} - ${descriptions[i]}"
done
echo -e "\nAre you sure you want install these packages? y/N ${RED}NOTE: Pray your package manager skips pre installed packages${NC}"

read ans

if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
	sudo $pkgmng update
	# Packages should be made into a proper list
	for i in "${packages[@]}" 
	do 
		echo -e "Installing ${BLUE}$i${NC}" 
		sudo $pkgmng install "$i"
		echo ""
	done
	
	# Download and install pip from get-pip.py
	echo -e "${LIGHTBLUE}Installing distro free pip3${NC}"
	curl https://bootstrap.pypa.io/get-pip.py -o ~/Downloads/get-pip.py
	python3 ~/Downloads/get-pip.py --user
	
	echo -e "${LIGHTBLUE}Installing youtube-dl${NC}"
	
	python3 -m pip install youtube-dl
	
	# Stop default apache to prevent conflicts with nginx-proxy
	sudo systemctl stop apache2
	sudo systemctl disable apache2
else
	echo -e "${RED}Exiting install script!${NC}"
	exit
fi
