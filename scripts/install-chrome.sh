#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

chromedlink="https://dl.google.com/linux/direct/google-chrome-stable_current_$pkgextension"
wget $chromedlink -P $HOME/Downloads

if [ "$distro" = "Ubuntu" ]; then
	sudo dpkg -i $HOME/Downloads/google-chrome-stable_current_$pkgextension
elif [ "$distro" = "Fedora" ]; then
	sudo rpm -U $HOME/Downloads/google-chrome-stable_current_$pkgextension
else
	echo "${RED}Skipping chrome installation Reason: Unsupported distro${NC}"
fi

