#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

postmandlink="https://dl.pstmn.io/download/latest/linux64"
postmanfilename="postman.tar.gz"
postmanoriginal=$(basname "$postmandlink")

wget -O $HOME/Downloads/$postmanfilename $postmandlink

sudo tar xvzf $HOME/Downloads/$postmanfilename -C /opt

touch $HOME/.local/share/applications/Postman.desktop

echo "[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=/opt/Postman/app/Postman %U
Icon=/opt/Postman/app/resources/app/assets/icon.png
Terminal=false
Type=Application
Categories=Development;" > $HOME/.local/share/applications/Postman.desktop

echo -e "${RED}Please check if postman has been installed!${NC}"
