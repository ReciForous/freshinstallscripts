#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

if [ "$distro" = "Ubuntu" ]; then
	sudo add-apt-repository ppa:ondrej/php
	sudo $pkgmng update

	# Install php
	sudo $pkgmng install php7.i3

	# Install php libraries
	sudo $pkgmng install php7.3-cli php7.3-fpm php7.3-json php7.3-pdo php7.3-mysql php7.3-zip php7.3-gd  php7.3-mbstring php7.3-curl php7.3-xml php7.3-bcmath php7.3-json
elif [ "$distro" = "Fedora" ]; then
	sudo $pkgmng install -y dnf-plugins-core
	sudo $pkmng config-manager --set-enabled remi-php73
	sudo $pkmng config-manager --set-enabled remi
	sudo $pkmng module install php:remi-7.3
fi

echo "${LIGHTBLUE}Installing composer!"
curl -sS https://getcomposer.org/installer -o $HOME/Downloads/composer-setup.php
sudo php $HOME/Downloads/composer-setup.php --install-dir=/usr/local/bin --filename=composer

if [ "$(composer -v)" ]; then
	echo -e "${GREEN}Composer install success!${NC}"
else
	echo -e "${RED}Composer install failed! Please install manually${NC}"
fi

