#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

skip="false"
if [ "$distro" = "Ubuntu" ]; then
	curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
elif [ "$distro" = "Fedora" ]; then
	curl -sL https://rpm.nodesource.com/setup_12.x | bash -
else
	skip="true"
fi

if [ ! "$skip" = "true" ]; then
	sudo $pkgmng -y install nodejs
	if [ "$(nodejs --version)" ]; then
		echo -e "${GREEN}Node has been successfully installed${NC}"
	else
		echo -e "${RED}Node has not been installed${NC}"
	fi

	if [ "$(npm --version)" ]; then
		echo -e "${GREEN}Npm has been successfully installed${NC}"
	else
		echo -e "${RED}Npm has not been installed${NC}"
	fi
else
	echo -e "Node installation has been skipped. ${RED}Reason: Unsupported distro${NC}"
fi
 			

