#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

# Configure docker repos distribution wise
if [ "$distro" = "Ubuntu" ]; then
	skip='False'
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo apt-key fingerprint 0EBFCD88
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
elif [ "$distro" = "Fedora" ]; then
	skip='False'
	sudo $pkgmng -y install dnf-plugins-core
	sudo $pkgmng config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
else
	skip='True'
	echo -e "${RED}Skipping docker installation! Reason: Unsupported distro${NC}"
fi

# Install docker with dependencies
if [ ! "$skip" = 'True' ]; then
	sudo $pkgmng update
	sudo $pkgmng install docker-ce docker-ce-cli containerd.io
fi

# Install docker-compose
echo -e "${LIGHTBLUE}Setting up docker-compose...${NC}"
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo -e "${GREEN}docker-compose setup successfull${NC}"

# Docker without sudo

# Fix out permissions before hand
echo -e "Run ${RED}docker run hello-world${NC} to check if your installation is working correctly!"
