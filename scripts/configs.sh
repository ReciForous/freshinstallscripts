
#!/bin/bash

distro="$(cat /etc/os-release | grep --max-count=1 NAME | sed 's/NAME=//' | sed 's/.//;s/.$//')"

pkgmng="NULL"
pkgextension="NULL"

if [ "$distro" = "Ubuntu"  ]; then
	pkgmng="apt"
	pkgextension="amd64.deb"
elif [ "$distro" = "Fedora"  ]; then
	pkgmng="dnf"
	pkgextenstion="x86_64.rpm"
fi

