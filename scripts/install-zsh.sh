#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

sudo $pkgmng install zsh
echo "Downloading oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
echo "Copying custom zsh files"

cp -r configs/.zshrc $HOME
cp -r configs/.oh-my-zsh $HOME
cp -r configs/.zsh_history $HOME

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
