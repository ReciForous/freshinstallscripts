#!/bin/bash

source scripts/configs.sh
source scripts/prettify.sh

skip="False"

if [ "$distro" = "Ubuntu" ]; then
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
elif [ "$distro" = "Fedora" ]; then
	sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
	sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
else
	skip="True"
	echo -e "${RED}Skipping sublime installation! Reason: Unsupported distro${NC}"
fi

if [ ! "$skip" = "True" ];then
	sudo $pkgmng update
	sudo $pkgmng install sublime-text
fi

