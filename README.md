# Fresh Install Scripts (To make an interns life easier)

## Description

An easy to run fresh install and custom shell configs with all the shit I need based on the linux distro.

This script will install the following applications:

	- Sublime
	- Mpv
	- Google Chrome
	- Postman

This script will install the following command line tools:

	- Git
	- MySql
	- Docker
	- Docker compose
	- PHP
	- Composer for PHP
	- Python3
	- Pip3 (Distro independent)
	- youtube-dl
	- vim
	- apt-transport-https (Ubuntu)
	- dnf-plugins-core (Fedora)
	- tmux
	- gnupg-agent (Ubuntu)
	- software-properties-common (Ubuntu)
	
## Instructions

```
# Copy paste these commands in terminal 

cd ~/freshinstallscripts

sudo chmod +x main.sh

./main.sh

# ===========================================
# Your pwd should look like this when you run main.sh
# /home/$USER/freshinstallscripts/
# ===========================================
# WARNING: DO NOT TRY TO RUN TO RUN SCRIPTS IN scripts/ SEPARATELY UNLESS YOU'RE WILLING TO REWRITE THE SCRIPT
# ===========================================
# Currently supported linux distros:
#	- Ubuntu
#	- Fedora
# ===========================================
# Follow on screen instructions from this point
# ===========================================
# If you want additional packages from repo edit scripts/install-packages.sh
# ===========================================
# Current packages to be installed are:
#	- mysql (mysql-server & mysql-community)
#	- python3
#	- git
#   - vim
#	- youtube-dl
#	- wget
#	- curl
#	- apt-transport-https
#	- ca-certificates
#	- gnupg-agent
#	- software-properties-common
#	- pip3
# ===========================================
```